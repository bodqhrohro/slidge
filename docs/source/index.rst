Welcome to SliXMPP gateway's documentation!
===========================================


.. toctree::
   :maxdepth: 2

   intro
   signal/index
   guide/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
